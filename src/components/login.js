import React from 'react'
import LoginFormContainer from '../containers/login-form'
import '../style/components/login.sass'
import BitBucket from './widgets/bitbucket'


const Login = (props) => {
  const hints = [
      'firstuser : 123123 (default user)',
      'Use POST to /api/auth/signup {username:, password:} to add users'
  ];

  return (
      <div className="module login">
        <BitBucket/>
        <div className="login-wrap">
          <LoginFormContainer {...props}/>
          <div className="hints">
            {
              hints.map((hint, i) => (
                  <div className="tip"
                       key={i}>Hint: {hint}</div>
              ))
            }
          </div>
        </div>
      </div>
  )
};

export default Login