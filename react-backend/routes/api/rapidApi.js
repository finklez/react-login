var express = require('express');
var router = express.Router();

var rapidReq = require('../../services/rapidApi');


router.get('/:apiKey', function (req, res, next) {
  rapidReq(req.params['apiKey'])
      .then(result => {
        console.log(result.status, result.body);
        res.json({success: true, res: result.body});
      });
});

module.exports = router;
