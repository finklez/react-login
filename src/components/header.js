import React from 'react'
import logo from '../assets/images/logo.svg'
import '../style/components/header.scss'
import { Signout } from '../components/auth'



export const Header = () => {
  return (
      <header className="app-header">
        <img src={logo} className="app-logo" alt="logo" />
        <h1 className="app-title">React Login API</h1>
        <Signout/>
      </header>
  );
}
