import React from 'react'
import {RouteAuth} from './auth'
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'
import Layout from './layout'
import User from './user'
import Login from './login'

const Routes = () => (
        <Router>
          <div>
            <Layout>
              <Route path="/login" component={Login}/>
              <RouteAuth exact path="/" component={User}/>
            </Layout>
          </div>
        </Router>
);

export default Routes
