TRAP_MSG = Trap, do nothing
BACKEND_DIR = react-backend

default: default_msg help

default_msg:
	@echo ${TRAP_MSG}
	@echo 'lets print help instead..'
	@echo

help:
	@echo 'Options:'
	@echo 'init        initialize npm modules'

init:
	npm install
	cd ${BACKEND_DIR}
	npm install
	cd -

build_hash:
	@echo temp workaround for service worker
	@echo print hashes for files in $$PWD/${BACKEND_DIR}/public
	@echo
	@cd ${BACKEND_DIR}/public && \
	for f in *; do [ -f "$$f" ] && md5sum "$$f" || echo -n; done