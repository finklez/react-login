var express = require('express');
var router = express.Router();
// var root = '../..';

// var User = require(root + "/models/user");

// router.get('/:username', function (req, res, next) {
//   User.find({
//     username: req.params['username']
//   }, function(err, user) {
//     // let userNames = users.map(function(u) {
//     //   return u['username'];
//     // });
//     res.json({success: true, user: user});
//   });
// });

router.get('/me', function (req, res, next) {
  const user = req.user;
  res.json({success: true, user: {
      id: user._id,
      username: user.username
    }});
});

module.exports = router;
