// see: https://parceljs.org/assets.html
import fs from 'fs';

// function cleaner(str) {
//   return str.replace(/(data|aria)-(\w)/g,
//       (whole, m1) => { return m1.toUpperCase(); });
// }

function innerSVG(str) {
  const el = document.createElement('div');
  el.innerHTML = str;
  return el.firstChild.innerHTML;
}

// TODO: how can we import files statically but dynamically? do we
// TODO:    need server rendering for that?
export default function Reader(file = 'spinner') {
  const content = fs.readFileSync(`${__dirname}/../assets/images/${'spinner'}.svg`, 'utf8'); // file must be statically evaluated
  return innerSVG(content);
}