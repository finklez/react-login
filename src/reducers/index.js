import { combineReducers } from 'redux'

import loginForm from './login-form'

const rootReducer = combineReducers({
  loginForm
});

export default rootReducer