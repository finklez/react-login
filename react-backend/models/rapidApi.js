var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RapidApiSchema = new Schema({
  serviceName: {
    type: String,
    unique: true,
    required: true
  },
  someOtherField: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model('User', RapidApiSchema);