
const Persistence = {
  key: 'access_token',

  save(data) {
    localStorage.setItem(this.key, data)
  },

  fetch() {
    return localStorage.getItem(this.key)
  },

  delete() {
    localStorage.removeItem(this.key);
  }
};

export default Persistence