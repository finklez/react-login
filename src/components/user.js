import React from 'react'
import Auth, { host } from '../lib/auth'
import '../style/components/user.sass';
import FamousQuote from './rapid-api/famous-quote'
import Draggable from 'react-draggable';
import BitBucket from './widgets/bitbucket'

class User extends React.Component {
  state = {
    user: null
  };

  componentWillMount() {
    this.setDraggable();
  }

  componentDidMount() {
    this.fetchDetails();
    this.rapidAPIFetch('famousQuotes');
  }

  // TODO: this should be handled in diff component
  setDraggable = () => {
    const dragPos = localStorage.getItem('draggableQuote');
    if(!dragPos) return;

    this.setState({dragPos: JSON.parse(dragPos)});
  };


  // TODO: move to redux/container
  fetchDetails = () => {
    let token = Auth.getToken();
    // console.log('token ', token);

    const details = {
      method: "GET",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json",
        "Authorization": token
      }
    };

    if(token) {
      if(!this.state.user) {
        return fetch(`${host}/api/user/me`, details)
            .then(res => res.json())
            .then(res => this.setState({ user: res.user }))
            .catch(err => console.log('API call error: ', err));
      }
    }

    this.setState({user: null});
  };

  // TODO: move to redux/container
  // TODO: make all api calls generic, move to api lib
  rapidAPIFetch = (key) => {
    let token = Auth.getToken();
    const details = {
      method: "GET",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json",
        "Authorization": token
      }
    };
    this.setState({ rapidQuote: {loading: true} });

    return fetch(`${host}/api/rapid/${key}`, details)
        .then(res => res.json())
        .then(res => this.setState({ rapidQuote: { ...res.res, loading: false} }))
        .catch(err => console.log('API call error: ', err));
  };

  // TODO: this should be handled in diff component
  handleStop = (e, data) => {
    const {x, y} = data, pos = {x, y}; // TODO: how-to single assignment?
    localStorage.setItem('draggableQuote', JSON.stringify(pos))
  };


  render() {
    const { user, rapidQuote, dragPos } = { dragPos: {x: 0, y: 50}, ...this.state };
    const rapidQuoteHint = `Fetching data from RapidAPI ${`famousQuotes`} through our Node gateway`;

    return (
        <div className="user module">
          <BitBucket/>
          {user ? <div className="welcome">
            Welcome {user.username}</div> : null}
          <Draggable
              handle=".handle"
              defaultPosition={dragPos}
              onStop={this.handleStop}>
            <div className="handle draggable">
              <FamousQuote {...rapidQuote}
                           hint={rapidQuoteHint}
                           fetchQuote={this.rapidAPIFetch}/>
              <div className="drag-me">Drag</div>
            </div>
          </Draggable>
        </div>
    );
  }
}

export default User
