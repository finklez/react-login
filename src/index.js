import React from 'react';
import './style/components/index.sass';
import Routes from './components/router';
// import registerServiceWorker from './registerServiceWorker';

import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers'

// let store = createStore(rootReducer);
const store = createStore(
    rootReducer,
    applyMiddleware(thunk)
);

render(
    <Provider store={store}>
        <Routes />
    </Provider>,
    document.getElementById('root')
);
// apparently service workers are only needed for mobile PWA. i don't really need
// this stuff atm, so it's currently disabled to save myself the trouble maintaining it
// registerServiceWorker(); // wow, Parcel doesn't play nice with that, so much trouble
                          // i had to go through
