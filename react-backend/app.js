var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var mongoose = require('mongoose');
var passport = require('passport');
var config = require('./config/database');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var yarm = require('yarm'); // unused atm

var index = require('./routes/index');
var apiAuth = require('./routes/api/auth');
var api = require('./routes/api');

//== lodash _. can be loaded in multiple ways:
// src: https://lodash.com/docs/4.17.4

// Load the full build.
// var _ = require('lodash');
// Load the core build.
// var _ = require('lodash/core');
// Load the FP build for immutable auto-curried iteratee-first data-last methods.
// var fp = require('lodash/fp');

// Load method categories.
// var array = require('lodash/array');
var _obj = require('lodash/object');
//============================

var app = express();

mongoose.connect(config.database);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade'); // TODO: change to pug

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// make this server CORS-ENABLE
// thanks here, see: https://gist.github.com/nilcolor/816580
// TODO: consider using the 'cors' module, see: https://github.com/expressjs/cors
app.use(function(req, res, next) {
  var headers = {};
  headers["Access-Control-Allow-Origin"] = "*";
  headers["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS, PUT, PATCH, DELETE";
  headers["Access-Control-Allow-Credentials"] = false; // whether to allow cookies, see:
  // https://stackoverflow.com/questions/24687313/what-exactly-does-the-access-control-allow-credentials-header-do

  if (req.method === 'OPTIONS') {
    // IE8 does not allow domains to be specified, just the * - who cares about IE8?
    // headers["Access-Control-Allow-Origin"] = req.headers.origin;
    headers["Access-Control-Max-Age"] = '86400'; // 24 hours TODO: do we need/want this?
    headers["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type, Accept, X-HTTP-Method-Override, Authorization";
    res.writeHead(200, headers);
    return res.end();
  }

  headers["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type, Accept, Origin, Authorization"; // TODO: why the diff?
                                                                                                      // TODO: Origin / X-HTTP-Method-Ov.. / Content-Length
  _obj.forOwn(headers, (header, value) => {
    res.header(value, header);
  });
  next();
});

// TODO: Solution B - which one do i like better?
// app.use(function(req, res, next) {
//   var headers = {};
//
//   // set header to handle the CORS
//   headers['Access-Control-Allow-Origin'] = '*';
//   headers['Access-Control-Allow-Headers'] = 'X-Requested-With, Content-Type, Accept, Content-Length, Authorization';
//   headers['Access-Contrl-Allow-Methods'] = 'PUT, POST, GET, DELETE, OPTIONS';
//   headers["Access-Control-Max-Age"] = '86400';
//   res.writeHead(200, headers);
//
//   if ( req.method === 'OPTIONS' ) {
//     console.log('OPTIONS SUCCESS');
//     res.end();
//   }
//   else {
//     //other requests
//   }
// }

app.use(passport.initialize());

// TODO: clean up routing, see: https://scotch.io/tutorials/keeping-api-routing-clean-using-express-routers
app.use('/api/auth', apiAuth);
app.use('/api', passport.authenticate('jwt'), api);
app.use('/*', index); // the '*' is for support for react routing

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;