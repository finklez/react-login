exports.up = function(next) {
  this
      .model('User')
      .create(
          {
            username: 'firstuser',
            password: '123123'
          },
          function (error, user) {
            if (error) {
              console.error(error);
            }
            console.log('user ', user);
            next();
          }
      );
};

exports.down = function(next) {
  this
      .model('User')
      .remove(
          {
            username: 'firstuser'
          },
          function (error, user) {
            if (error) {
              console.error(error);
            }
            console.log('user ', user);
            next();
          }
      );
};