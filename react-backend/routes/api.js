var express = require('express');
var router = express.Router();

var users = require('./api/users');
var rapid = require('./api/rapidApi');

router.use('/user', users);
router.use('/rapid', rapid);


module.exports = router;