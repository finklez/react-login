// just remember that the reducer must be pure. Given the same arguments, it should calculate
//  the next state and return it. No surprises. No side effects. No API calls. No mutations. Just a calculation.
// see: https://redux.js.org/docs/basics/Reducers.html

// https://redux.js.org/docs/advanced/

import {INIT_LOGIN, LOGIN_RESPONSE} from "../actions/login-form";

const initialState = {
  isFetching: false,
  success: null,
  msg: ''
};

const loginForm = (state = initialState, action) => {
  switch (action.type) {
    case INIT_LOGIN:
      return {...state, ...initialState, isFetching: true};
    case LOGIN_RESPONSE:
      return {...state, ...initialState, ...action.res};
    default:
      return state
  }
};

export default loginForm