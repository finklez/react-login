import Auth from "../lib/auth";

export const INIT_LOGIN = 'INIT_LOGIN';

function initLogin(userPass) {
  return {
    type: INIT_LOGIN,
    userPass
  }
}

export const LOGIN_RESPONSE = 'LOGIN_RESPONSE';

function loginResponse(userPass, res) {
  return {
    type: LOGIN_RESPONSE,
    userPass,
    res: res
  }
}

// thunk action creator
// Do not use catch, because that will also catch
// any errors in the dispatch and resulting render,
export function logIn(userPass) {
  return function (dispatch) {
    dispatch(initLogin(userPass));

    return Auth.authenticate(userPass)
        .then(res =>
            dispatch(loginResponse(userPass, res))
        )
  }
}
