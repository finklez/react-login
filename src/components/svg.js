import React from 'react'
import SVGReader from '../lib/svgReader'

// TODO: this needs some work. can we inject svg directly in JSX before parsing?
// TODO: grab the orig attrs (i.e viewBox)
const SVG = ({className, wrapperClass}) => (
    <span className={`svg-wrap ${wrapperClass}`}>
      <svg className={`svg-icon ${className}`}
           viewBox="0 0 512 512"
           dangerouslySetInnerHTML={{__html: SVGReader()}}/>
    </span>
);

export default SVG