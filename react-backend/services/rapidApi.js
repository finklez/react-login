// see: http://unirest.io/nodejs.html
var unirest = require('unirest');
var RapidAPI = require('rapidapi-connect');

const PROJECT_NAME = 'default-application_5a722ac2e4b00687d356c33d',
    PROJECT_KEY = '9361bfa6-cef6-4750-aa07-3c89d3b8f251'
;

const rapid = new RapidAPI(PROJECT_NAME, PROJECT_KEY);

const APIs = {
  famousQuotes: {
    url: "https://andruxnet-random-famous-quotes.p.mashape.com/?cat=famous&count=10",
    key: "QGEUerv7BWmshxcNDuwgUtn5PVP7p14gTNKjsnNbKKMJHkxPro",
    host: "andruxnet-random-famous-quotes.p.mashape.com"
  }
};

function getApiByKey(apiKey) {
  return APIs[apiKey];
}

function request(apiKey) {
  /*
  rapid.call('PackageName', 'FunctionName', {
    'ParameterKey1': 'ParameterValue1',
    'ParameterKey2': 'ParameterValue2'
  })
  .then(
      payload => {},
      error => {}
  );
  */
  const params = getApiByKey(apiKey);

  const uni = unirest.get(params.url)
      .header("X-Mashape-Key", params.key)
      .header("X-Mashape-Host", params.host);
      // .send({ "parameter": 23, "foo": "bar" })

  return new Promise((resolve, reject) => {
        uni.end(result => {
          // console.log(result.status, result.headers, result.body);
          resolve(result);
        });
      });
}
/*
RESPONSE:
  quote:"I don't know why we are here, but I'm pretty sure that it is not in order to enjoy ourselves."
  author:"Ludwig Wittgenstein"
  category:"Famous"
 */

module.exports = request;