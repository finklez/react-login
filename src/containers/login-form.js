import { connect } from 'react-redux'
import { logIn } from '../actions/login-form'
import LoginForm from '../components/login-form'

const mapStateToProps = state => {
  return {
      loginRequest: state.loginForm
  }
};

const mapDispatchToProps = dispatch => {
  return {
    logIn: (fields) => {
      dispatch(logIn(fields))
    }
  }
};

const LoginFormContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginForm);

export default LoginFormContainer