import authPlug from "../components/auth";

export const sendPost = id => {
  return {
    type: 'SEND_POST',
    id
  }
};

//
// export const fetchDetails = () => {
//   let token = authPlug.getToken();
//   console.log('token ', token);
//
//   const details = {
//     method: "GET",
//     credentials: "same-origin",
//     headers: {
//       "Content-Type": "application/json",
//       "Authorization": token
//     }
//   };
//
//   if(token) {
//     if(!this.state.user) {
//       fetch(`${config.proxy}/api/user/me`, details)
//           .then(res => res.json())
//           .then(user => this.setState({ user }));
//     }
//   } else {
//     this.setState({user: null});
//   }
// };
