import '../style/components/login-form.sass'

import React from 'react'
import {
  Redirect,
} from 'react-router-dom'
import _coll from 'lodash/collection'
import SVG from './svg'

const validStateFields = (valid) => {
  return {
    valid,
    validMessage: valid ? '' : 'Please fill in all fields'
  }
};


class LoginForm extends React.Component {
  state = {
    redirectToReferrer: false,
    fields: {
      username: '',
      password: ''
    },
    valid: true,
    validMessage: ''
  };

  handleSubmit = (e) => {
    e.preventDefault();

    this.validate()
        .then(
            () => this.props.logIn({ ...this.state.fields }),
            error => { console.log('error: ', error) }
        )
  };

  validate = () => {
    let valid = !_coll.includes(this.state.fields, '');

    return new Promise((resolve, reject) => {
      this.setState({...validStateFields(valid)}, () => {
        valid ? resolve() : reject('error in field {placeholder}');
      });
    });
  };

  onInputChange = (e) => {
    // oo style
    // let state = Object.assign({}, this.state, {
    //   fields: {[e.target.name]: e.target.value},
    //   valid: true
    // });

    // functional style, which one do i like better?
    let target = e.target; // since it's async we must localize the data
    this.setState(state => {
      state.fields[target.name] = target.value;
      return {...state, ...validStateFields(true)};
    });
  };

  handleLoginRequest = (nextReqProps) => {
    this.setState(
        (() => {
          if(!nextReqProps.success) {
            return {
              valid: false,
              validMessage: nextReqProps.msg,
              redirectToReferrer: false
            }
          }

          return {
            valid: true,
            validMessage: '',
            redirectToReferrer: true
          }
        })()
    )
  };

  componentWillReceiveProps(nextProps) {
    const {loginRequest: nextReqProps} = nextProps;

    if(nextReqProps.success !== null) {
      this.handleLoginRequest(nextReqProps)
    }
  }

  render() {
    // return null;
    const { from } = this.props.location.state || { from: { pathname: '/' } },
        { redirectToReferrer } = this.state,
        { isFetching } = this.props.loginRequest
    ;

    if (redirectToReferrer) {
      return (
          <Redirect to={from}/>
      )
    }

    return (
        <form className={`login-form${isFetching ? ' in-progress' : ''}`} onSubmit={this.handleSubmit}>
          <p>
            Login
          </p>
          <p>
            <input type="text" name="username" placeholder="Username"
                   onChange={this.onInputChange} />
          </p>
          <p>
            <input type="password" name="password" placeholder="Password"
                   onChange={this.onInputChange} />
          </p>
          <div className={`alert-box${this.state.valid ? '' : ' error'}`}>
            {this.state.validMessage}
          </div>
          <p className="top-vspace-none">
            <button type="submit" disabled={isFetching}>Log in
              <SVG className="svg-spin animated"
                   wrapperClass="loader icon"/>
            </button>
          </p>
        </form>
    )
  }
}

export default LoginForm
