import React from 'react'
import '../../style/components/widgets/bitbucket.sass';

const bitBucketURL = 'https://bitbucket.org/finklez/react-login';

// TODO: this should be in diff component
// TODO: move svg to file, use the reader
const BitBucket = () => (
    <div className="bitbucket">
      <span className="svg-wrap icon">
        {/* crudely ripped from their site.. */}
        <svg className="svg-icon" viewBox="0 40 63.43 68.26">
          <defs>
            <linearGradient id="logo-gradient" x1="64.01" y1="65.26" x2="32.99" y2="89.48" gradientUnits="userSpaceOnUse">
              <stop offset="0.18" stopColor="#0052cc"/>
              <stop offset="1" stopColor="#2684ff"/>
            </linearGradient>
          </defs>
          <g className="header__svg-logo__icon">
            <path className="header__svg-logo__icon--path--3" d="M2,41.25a2,2,0,0,0-2,2.32L8.49,95.11a2.72,2.72,0,0,0,2.66,2.27H51.88a2,2,0,0,0,2-1.68l8.51-52.11a2,2,0,0,0-2-2.32ZM37.75,78.5h-13L21.23,60.12H40.9Z"/>
            <path className="header__svg-logo__icon--path--4" d="M59.67,60.12H40.9L37.75,78.5h-13L9.4,96.73a2.71,2.71,0,0,0,1.75.66H51.89a2,2,0,0,0,2-1.68Z"/>
          </g>
        </svg>
      </span>
      <a href={bitBucketURL}
         target="_blank">Visit the project on BitBucket</a>
      <div className="tip">Read the README, follow the Commits</div>
    </div>
);

export default BitBucket