import {
  withRouter,
  Redirect,
  Route
} from "react-router-dom"
import React from 'react'
import authPlug from '../lib/auth'

// TODO: move logic outside view, maybe to container
// TODO: add user name
export const Signout = withRouter(({ history }) => (
    authPlug.isAuthenticated() ? (
        <a className="signout" onClick={() => {
          authPlug.signout(() => history.push('/'))
        }}>Sign out</a>
    ) : null
));

export const RouteAuth = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        authPlug.isAuthenticated() ? (
            <Component {...props}/>
        ) : (
            <Redirect to={{
              pathname: '/login',
              state: { from: props.location }
            }}/>
        )
    )}/>
);
