import React from 'react'
import {Header} from './header'

class Layout extends React.Component {

  render() {
    return (
        <div className="app">
          <Header />
          <div className="wrapper">
            {this.props.children}
          </div>
        </div>
    )
  }
}

export default Layout
