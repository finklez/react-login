import '../../style/components/rapid-api/famous-quote.sass'
import React from 'react'

// TODO: add loader
const FamousQuote = ({author, category: cat, quote, fetchQuote, hint, loading}) => {
  return (
      <div className={`famous-quote ${quote ? 'has-quote' : ''}${loading ? ' loading' : ''}`}>
        <div className="hint">{hint}</div>
        <button className="fetch-quote btn btn-info"
                onClick={() => {fetchQuote('famousQuotes')}}>Fetch</button>
        <div className="quote">{loading ? 'loading..' : quote}</div>
        <div>
          <span className="author">{author}</span>
          <span className="cat">{cat}</span>
        </div>
      </div>
  )
};

export default FamousQuote
