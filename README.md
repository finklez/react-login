## React Login Challenge

See it live at: https://react-login-api.herokuapp.com/

#### About
The initial goal was to create a simple React app consisting of a login page and another page that requires authentication, with a logout button and a draggable item and instantiate an API call to third party. I took this opportunity to get familiar with Node, Redux and the Node ecosystem in general, and build on this premise a full stack Express framework

#### It's all about evolution
Two weeks ago i knew nothing about Node, heck, i only vaguely understood how npm work or what package.json is, and i won't even mention Express. I do have some React experience, but have never created a React project from scratch, and Redux was just another buzzword. Even ES6, tho I'm a bit ashamed to admit.. Well, that was then.

I started this project with [Create React App](https://www.npmjs.com/package/create-react-app), as was suggested in the [React starter kits](https://reactjs.org/community/starter-kits.html) section, i went with one of the simplest kits as to not get overwhelmed, bit by bit i started adding components, copy-pasting pieces of code from the net, learning, as i made sense out of all the new stuff, i started shaping the code to my needs and molding everything together, the result is what you see today.

**How to evaluate this project**

One might open a browser and be utterly underwhelmed, "there's only two simple pages", but the presentation is only a small part of the story, this project is actually 90% framework, so one might look at the code and say "ok", but the real story here is how the project evolved from a boilerplate to a final product; the correct way to look at this project is to view the commits, see what changes i made and why, the "before" and "after"; considering that two weeks ago i was clueless, i'm pretty happy with the result.

<i>One can even use this project as a boilerplate for a new Express+MongoDB+React+Redux project</i>

#### So what did we learn?
In short, A lot. If we want to get specific, lets list some:
- React / Node ecosystem
- Markdown, ye, even this README file
- ES6 basics, destructors, spread, classes, promise, import/export
- React, JSX, just a bit better
- Redux (finally), containers, reducers, actions, connect
- Parcel bundler (Webpack replacement), build assets
- NVM, RVM, package.json (deps, scripts)
- lodash, awesome JS tooling lib
- Node, just the beginning..
- Express, basic stuff, routing, controller, models
- MongoDB basics, mongoose ODM, data migration
- Building a React & Express project from scratch, Create React App
- Deploy from scratch to Heroku

**Other worth mentions**
- Redux Thunk middleware, async calls
- React Router
- Service Worker, PWA in general
- Passport, node authentication middleware
- Jade/Pug, well i already knew Haml

It's been crazy 2 weeks, it has been awesome, i'm learning a lot, Node is pretty cool. I'm not a Node developer just yet, but i now know enough to get started, and i will definitely put it to use.


#### Project structure (**subject to change)
\*\* [Now there's a temp sister project for deploy](https://bitbucket.org/finklez/node-react-api)

The project is composed of 2 main components (directories):
- / (root) - client code in React
- react-backend - server code in Node **

\* Each component has its own package.json file

\*\* Having Node in a subdir was a mistake (if the intention is to serve React thru node). Node is the project to be deployed and should reside on the main dir, React after build will consist of only few static files that will sit in the Node public dir.

this was especially a prob with Heroku (you just push, it takes care of the rest), since it expects your project to be in the root dir, so i had to create a new git repo with Node as root

I'm undecided yet if Node-React should be a single project or each on its own dir.


#### Install
```
git clone https://bitbucket.org/finklez/react-login

cd react-login

make init   # installs both client & server npm modules
```

#### Getting Started
**tl;dr**

1. To get started quickly, run the following command:

        npm run dev
    this will init **MongoDB**, **Node/Express**, and **React server** automagically for you.

    \* Alas, if on the other hand, you prefer to run each service on its own console, skip ahead to the script section

1. Run database migrations

        npm run migrate

1. Open the browser (if it didn't happened automatically) on:

        http://localhost:1234

    \* The server can be found on port 3001

**To list available scripts run:**

\* assuming `./node_modules/.bin` is in your PATH (hint)

    ls-scripts

\** if not, add `export PATH=./node_modules/.bin:$PATH` to your bash script

<br>

**Run scripts with:**

    npm run <script_name>

<br>

**Available scripts:** (see next for per-script details)

- **start**         - Run React server in development mode
- **build**         - Build the react project for production
- **server-dev**    - Run Node server in development mode
- **server**        - Run Node server in production mode
- **mongod**        - Run MongoDB daemon
- **dev**           - Run MongoDB, React server, and Node concurrently
- **prod**          - Run MongoDB, React server concurrently
- **migrate**       - Run database migrations

\* the server scripts can also be run from the react-server dir

**Scripts in detail**

- **start** -
    Or simply `npm start`

    will run React with Hot loading using Parcel bundler

- **build** -
    the bundled assets (and html) can be found in `react-server/public/.dist folder`

- **server-dev** -
    Just like React, this will run with Hot loading using `nodemon`

- **mongod** -
    Set with non standard port in order to not collide with any locally running instances

- **dev, prod** -
    These two guys are using 'concurrently' module to run multiple processes in parallel

- **migrate** -
    Used to insert pre-defined data

**If you are creating new scripts..**

you can list all env variables available to your script by running `npm run env`

#### Modules / Packages used
- client
    - react - [React is a JavaScript library for building user interfaces](https://www.npmjs.com/package/react)
    - redux - [Predictable state container for JavaScript apps](https://www.npmjs.com/package/redux)
    - redux thunk - [Thunk middleware for Redux](https://www.npmjs.com/package/redux-thunk)
    - babel - [Babel puts a soft cushion between you all the cool new file formats being developed for node.js such as CoffeeScript, SASS, and Jade]()
    - react router - [Declarative routing for React](https://www.npmjs.com/package/react-router)
    - parcel - [Blazing fast, zero configuration web application bundler](https://www.npmjs.com/package/parcel-bundler)
    - react draggable - [A simple component for making elements draggable](https://www.npmjs.com/package/react-draggable)
    - rapidapi connect - [Connect to blocks on the rapidapi.com marketplace](https://www.npmjs.com/package/rapidapi-connect)
    - react scripts - [Configuration and scripts for Create React App](https://www.npmjs.com/package/react-scripts)
    - create react app - [Create React apps with no build configuration](https://www.npmjs.com/package/create-react-app)

- server
    - express - [Fast, unopinionated, minimalist web framework for node](https://www.npmjs.com/package/express)
    - mongoose - [MongoDB object modeling tool](https://www.npmjs.com/package/mongoose)
    - mongoose migration - [Data migration tool for Mongoose](https://www.npmjs.com/package/mongoose-migration)
    - morgan - [HTTP request logger middleware for node.js](https://www.npmjs.com/package/morgan)
    - yarm - [Yet Another REST Middleware for node.js, Express and mongoose](https://www.npmjs.com/package/yarm)
    - nodemon - [Simple monitor script for use during development of a node.js app](https://www.npmjs.com/package/nodemon)
    - passport - [Simple, unobtrusive authentication for Node.js](https://www.npmjs.com/package/passport)
    - passport-jwt - [Passport authentication strategy using JSON Web Tokens](https://www.npmjs.com/package/passport-jwt)

- generic
    - concurrently - [Run multiple commands concurrently](https://www.npmjs.com/package/concurrently)
    - lodash - [Lodash modular utilities](https://www.npmjs.com/package/lodash)

#### How To Build For Production
1. Run the build command

        npm run build

    this will compile the React project into the `react-backend/public/dist` folder
1. From here you have 2 options:

    1. If you just want to serve React as a static site, you can serve the bundled project using the `serve` module

    1. If on the other hand, you want the whole Express experience:

        1. Copy the `dist` folder **content** into the parent `public` folder

        1. Deploy the `react-backend` dir (* see the sister project mentioned above)

    You can also test it locally, if you still have the servers running, open the browser and use the server port:

        http://localhost:3001

#### TODOs
- replace Jade templates with Pug
- add i18n to handle texts
- add clean routes in Express
- add [Immutable](https://github.com/facebook/immutable-js/)
- unit / integration tests, jasmine / jest
- mobile first
- add propTypes
- defer asset loading
- switch client <=> server folder location (make Node main)
- call our RapidAPI route, print to screen
- connect to RapidAPI as a service
- re-organize components between pages and 'widgets'

#### FIXMEs
- generate service-worker on-the-fly, sadly it seems Parcel doesn't know how to handle it, so we are generating the file hashes manually
- check why react `proxy` stopped working when we moved to Parcel
- check why `Parcel --public_url` not working with `process.env.PUBLIC_URL`, tmp workaround set PUBLIC_URL in env
- check why migrations didn't work on Heroku

#### Sources
- https://reactjs.org/docs/add-react-to-a-new-app.html
- https://reacttraining.com/react-router/web/example/auth-workflow
- https://daveceddia.com/create-react-app-express-backend
- https://www.djamware.com/post/58eba06380aca72673af8500/node-express-mongoose-and-passportjs-rest-api-authentication
- https://www.npmjs.com/package/mongoose-migration
- https://docs.mongodb.com/manual/reference/sql-comparison/
- https://medium.appbase.io/how-to-implement-authentication-for-your-react-app-cf09eef3bb0b
- https://scotch.io/tutorials/keeping-api-routing-clean-using-express-routers
- https://github.com/erikras/react-redux-universal-hot-example/blob/master/webpack/dev.config.js
- https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#deployment
- https://devcenter.heroku.com/articles/node-best-practices#hook-things-up

#### Issues i encountered \[Solved\]
- https://github.com/bradtraversy/meanauthapp/issues/9

  passport-jwt package has been is now in version 3.0.* fromAuthHeader which was used in version 2 has been replaced with fromHeaderWithScheme(auth_scheme). In this case the auth_scheme is "jwt"

- https://github.com/auth0/node-jsonwebtoken/issues/403

    version new jsonwebtoken error #403

- https://stackoverflow.com/questions/19948816/error-failed-to-serialize-user-into-session

    Error: failed to serialize user into session

- https://github.com/parcel-bundler/parcel/issues/221

    Working with editors/IDEs supporting “safe write”<br>
    [Hot Module Replacement - Safe Write](https://parceljs.org/hmr.html)

#### Installation errors
- https://stackoverflow.com/questions/26768082/npm-install-mongoose-fails-kerberos-and-bson-errors
```bash
npm install mongoose-migration --save
sudo apt install libkrb5-dev # req on linux
```
