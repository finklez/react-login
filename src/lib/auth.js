// import history from '../components/history';

import Persistence from "./persistence";
import config from '../../package'
import fetch from 'cross-fetch' // used as a ponyfill (that's not a typo), but i'm not sure i like that idea
                                // isn't it better to use native API when possible?

const requestDetails = {
  method: "POST",
  // whether to send cookies, basic auth, see: https://developer.mozilla.org/en-US/docs/Web/API/Request/credentials
  credentials: "same-origin", // only use cookies on same origin, not on cross domain
  headers: {
    "Content-Type": "application/json"
  }
};

export const host = (
  process.env.NODE_ENV === 'production' ? '' : config.proxy
    // 'https://react-login-api.herokuapp.com'
);

export default class Auth {

  static isAuthenticated() {
    return !!Persistence.fetch();
  }

  static getToken() {
    return Persistence.fetch();
  }

  static authenticate(data) {
    const reqDetails = { ...requestDetails, body: JSON.stringify(data) };

    return fetch(`${host}/api/auth/signin`, reqDetails)
        .then(res => res.json())
        .then(
            res => {
              if(res.success) {
                Persistence.save(res.token);
              }
              return res;
            },
            err => {
              console.log('Sign in error: ', err);
            }
        )
  }

  static signout(cb) {
    Persistence.delete();
    cb();
  }

  //== NOT USED, TEMP KEEP FOR CODE IDEAS
  // src: auth0

  /*
  // Please use your own credentials here
  auth0 = new auth0.WebAuth({
    domain: 'divyanshu.auth0.com',
    clientID: 'TJyKPI6aRiRwgr6SxlT7ExW10NEHW4Vy',
    redirectUri: process.env.NODE_ENV === 'development' ? 'http://localhost:3000/callback' :
        'https://react-api',
    audience: 'https://divyanshu.auth0.com/userinfo',
    responseType: 'token id_token',
    scope: 'openid'
  });

  login = () => {
    this.auth0.authorize();
  };

  // parses the result after authentication from URL hash
  handleAuthentication = () => {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
        history.replace('/home');
      } else if (err) {
        history.replace('/home');
        console.log(err);
      }
    });
  };

  // Sets user details in localStorage
  setSession = (authResult) => {
    // Set the time that the access token will expire at
    let expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
    // navigate to the home route
    history.replace('/home');
  }

  // removes user details from localStorage
  logout = () => {
    // Clear access token and ID token from local storage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    // navigate to the home route
    history.replace('/home');
  };

  // checks if the user is authenticated
  isAuthenticated = () => {
    // Check whether the current time is past the
    // access token's expiry time
    let expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }
  */
}